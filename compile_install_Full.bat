@echo off
cls

REM loading visual studio environment
set G4_bat_file_dir=%~dp0
cd %G4_bat_file_dir%
call vsenv.bat

set G4_bat_file_dir=%~dp0

call "%G4_bat_file_dir%compile_install_Xersesc.bat"

call "%G4_bat_file_dir%compile_install_Qt5.bat"

call "%G4_bat_file_dir%compile_install_Geant4.bat"

REM call "%G4_bat_file_dir%compile_install_CADMesh.bat"

call "%G4_bat_file_dir%compile_install_ExampleB1.bat"

call "%G4_bat_file_dir%Deploy_ExampleB1.bat"

PAUSE
