@echo off
REM load environement variables (local to this script)
set G4_bat_file_dir=%~dp0
set PATH=%PATH%;%G4_bat_file_dir%install\bin;%G4_bat_file_dir%install\lib;%G4_bat_file_dir%install\include\Geant4
set PATH=%PATH%;%QTDIR%lib;%QTDIR%bin;%QTDIR%include;%QTDIR%plugins;%QTDIR%plugins\platforms;%QTDIR%lib\cmake
set PATH=%PATH%;%G4_bat_file_dir%xerces-c\install\bin;%G4_bat_file_dir%xerces-c\install\include;%G4_bat_file_dir%xerces-c\install\lib;%G4_bat_file_dir%xerces-c\cmake

REM launch CMake
set G4_bat_file_dir=%~dp0
%G4_bat_file_dir%\cmake-3.14\bin\cmake-gui.exe