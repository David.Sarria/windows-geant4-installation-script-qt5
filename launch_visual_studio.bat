@echo off
REM load environement variables (local to this script)
set G4_bat_file_dir=%~dp0
set PATH=%PATH%;%G4_bat_file_dir%install\bin;%G4_bat_file_dir%install\lib;%G4_bat_file_dir%install\include\Geant4
set PATH=%PATH%;%QTDIR%lib;%QTDIR%bin;%QTDIR%include;%QTDIR%plugins;%QTDIR%plugins\platforms;%QTDIR%lib\cmake
set PATH=%PATH%;%G4_bat_file_dir%xerces-c\install\bin;%G4_bat_file_dir%xerces-c\install\include;%G4_bat_file_dir%xerces-c\install\lib;%G4_bat_file_dir%xerces-c\cmake

REM launch visual studio IDE
set G4_bat_file_dir=%~dp0
@for /f "usebackq delims=#" %%a in (`"%G4_bat_file_dir%vswhere" -latest -property installationPath`) do "%%a\Common7\IDE\devenv.exe"