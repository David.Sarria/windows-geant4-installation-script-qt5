@echo off
cls
set G4_bat_file_dir=%~dp0
call :sub >%G4_bat_file_dir%\logs\compile_install_Xersesc.txt
exit /b
:sub

REM loading visual studio environment
set G4_bat_file_dir=%~dp0
cd %G4_bat_file_dir%
call vsenv.bat

REM setx is for permanent (future commands)
REM set is local (only this command execution)

set build_type=Debug

REM Compile and install XERSEC-C
echo Compiling and Installing XERSEC-C
cd %G4_bat_file_dir%

if not exist %G4_bat_file_dir%\xerces-c\install mkdir %G4_bat_file_dir%\xerces-c\install
if not exist %G4_bat_file_dir%\xerces-c\build mkdir %G4_bat_file_dir%\xerces-c\build

cd %G4_bat_file_dir%\xerces-c\build

REM if exist CMakeCache.txt del CMakeCache.txt

%G4_bat_file_dir%\cmake-3.14\bin\cmake.exe -DCMAKE_INSTALL_PREFIX=%G4_bat_file_dir%\xerces-c\install ^
-DCMAKE_CONFIGURATION_TYPES=%build_type%;Debug ^
-DBUILD_SHARED_LIBS=OFF ^
-DBUILD_STATIC_LIBS=ON ^
%G4_bat_file_dir%\xerces-c\source

%G4_bat_file_dir%\cmake-3.14\bin\cmake.exe --build . --config %build_type% --parallel 4
%G4_bat_file_dir%\cmake-3.14\bin\cmake.exe --build . --config %build_type% --target install

%G4_bat_file_dir%\cmake-3.14\bin\cmake.exe --build . --config Debug --parallel 4
%G4_bat_file_dir%\cmake-3.14\bin\cmake.exe --build . --config Debug --target install

echo DONE !

REM PAUSE
