@echo off
cls

set G4_bat_file_dir=%~dp0
call :sub >%G4_bat_file_dir%\logs\compile_install_ExampleB1.txt
exit /b
:sub

REM loading visual studio environment
set G4_bat_file_dir=%~dp0
cd %G4_bat_file_dir%
call vsenv.bat

set build_type=Debug

set G4_bat_file_dir=%~dp0

set Geant4_DIR=%G4_bat_file_dir%install\lib\Geant4-10.4.3\
set QTDIR=%G4_bat_file_dir%Qt5\install\
set QMAKESPEC=win32-msvc2017

set PATH=%PATH%;%G4_bat_file_dir%install\bin;%G4_bat_file_dir%install\lib;%G4_bat_file_dir%install\include\Geant4
set PATH=%PATH%;%QTDIR%lib;%QTDIR%bin;%QTDIR%include;%QTDIR%plugins;%QTDIR%plugins\platforms;%QTDIR%lib\cmake
set PATH=%PATH%;%G4_bat_file_dir%CADMesh\install\bin\lib;%G4_bat_file_dir%CADMesh\install\include;%G4_bat_file_dir%CADMesh\install\include\assimp;%G4_bat_file_dir%CADMesh\install\include\assimp\Compiler

cd %G4_bat_file_dir%

echo "TEST: Compiling and executing example B1"

if not exist %G4_bat_file_dir%\ExampleB1\build mkdir %G4_bat_file_dir%\ExampleB1\build

cd %G4_bat_file_dir%\ExampleB1\build

REM if exist CMakeCache.txt del CMakeCache.txt

%G4_bat_file_dir%\cmake-3.14\bin\cmake.exe ^
-DCMAKE_CONFIGURATION_TYPES=%build_type% ^
-DCMAKE_PREFIX_PATH=%G4_bat_file_dir%\install\lib\Geant4-10.4.3 ^
-DBUILD_SHARED_LIBS=OFF ^
-DBUILD_STATIC_LIBS=ON ^
%G4_bat_file_dir%\ExampleB1\source

%G4_bat_file_dir%\cmake-3.14\bin\cmake.exe --build . --config %build_type% --parallel 4

REM copying all the .mac files for visualization set up
for /R %G4_bat_file_dir%\ExampleB1\build\ %%f in (*.mac) do copy %%f %G4_bat_file_dir%\ExampleB1\build\%build_type%\

REM using windeployqt to put along exampleB1.exe the required DLL so that it can run and be deployed
start "" %G4_bat_file_dir%Qt5\install\bin\windeployqt.exe %G4_bat_file_dir%\ExampleB1\build\%build_type%\exampleB1.exe

REM launch exampleB1
start "" %G4_bat_file_dir%\ExampleB1\build\%build_type%\exampleB1.exe

REM PAUSE
