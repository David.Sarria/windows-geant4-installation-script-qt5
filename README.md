**DO NOT EXECUTE these .bat files if you don't want to rebuild everything from scratch**

## WINDOWS GEANT4 INSTALLATION SCRIPT

### Batch (.bat) scripts to compile and install GEANT4 on Windows with full functionallity (GDML, OpenGL, Qt). EXPERIMENTAL: probably still a few bugs. It may also mess up the Environment Variables... Use at your own risk.

* Requires Visual Studio 2017 (version 15.x). The installer is provided (vs_community.exe). Install with visual C++ support. **It is worth running the installer twice to make sure C++ support is installed correctly**.

* To compile and install everything, right click on `compile_install_Full.bat` and "Run as administrator". Will take at least 1.5 hour to compile and install... If everything works fine, it should compile and execute example B1. **If it crashes, try to execute again exampleB1.exe located at** `{root}/ExampleB1/build/Debug/`

* **It will create or update environement variables for the Geant4 datasets, Geant4 itself, Qt and CADMesh. So it could mess up your own Geant4 and/or Qt5 installation.**

* Use `launch_visual_studio.bat` to launch Visual Studio IDE with the correct environment
* Use `launch_CMake_Geant4.bat` to launch CMake-gui with the correct environment

* It will take a disk space of about 15 GB when finished. A lot of files are temporary compilation products and source code (only ~4.6 GB is mandatory).