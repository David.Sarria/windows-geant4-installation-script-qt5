@echo off
cls

set G4_bat_file_dir=%~dp0
call :sub >%G4_bat_file_dir%\logs\Deploy_ExampleB1.txt
exit /b
:sub

REM loading visual studio environment
set G4_bat_file_dir=%~dp0
cd %G4_bat_file_dir%
call vsenv.bat

set build_type=Debug

set G4_bat_file_dir=%~dp0

set Geant4_DIR=%G4_bat_file_dir%install\lib\Geant4-10.4.3\
set QTDIR=%G4_bat_file_dir%Qt5\install\
set QMAKESPEC=win32-msvc2017

REM copying all the .mac files for visualization set up
for /R %G4_bat_file_dir%\ExampleB1\build\ %%f in (*.mac) do copy %%f %G4_bat_file_dir%\ExampleB1\build\%build_type%\

REM copying missing DLL files to work in a computer without visual studio installed
for /R %G4_bat_file_dir%missing_dlls\ %%f in (*.dll) do copy %%f %G4_bat_file_dir%\ExampleB1\build\%build_type%\

REM using windeployqt to put along exampleB1.exe the required DLL so that it can run and be deployed
start "" %G4_bat_file_dir%Qt5\install\bin\windeployqt.exe %G4_bat_file_dir%\ExampleB1\build\%build_type%\exampleB1.exe

echo Deployment DONE

REM launch exampleB1

cd %G4_bat_file_dir%\ExampleB1\build\%build_type%\

start "" %G4_bat_file_dir%\ExampleB1\build\%build_type%\exampleB1.exe

echo DONE

REM PAUSE
